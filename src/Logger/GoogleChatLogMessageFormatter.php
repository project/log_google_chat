<?php

namespace Drupal\log_google_chat\Logger;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Render\Renderer;
use Psr\Log\LoggerInterface;

/**
* Logger class for log_google_chat module.
*/
class GoogleChatLogMessageFormatter implements LoggerInterface {
  use RfcLoggerTrait;

  /**
  * Configuration object.
  *
  * @var \Drupal\Core\Config\Config
  */
  protected $config;

  /**
  * Renderer object.
  *
  * @var \Drupal\Core\Render\Renderer
  */
  protected $renderer;

  /**
  * Constructs a SysLog object.
  *
  * @param \Drupal\Core\Config\ConfigFactory $config_factory
  *   The configuration factory object.
  * @param \Drupal\Core\Render\Renderer $renderer
  *   The Renderer object.
  */
  public function __construct(ConfigFactory $config_factory, Renderer $renderer) {
    $this->config = $config_factory->get('log_google_chat.settings');
    $this->renderer = $renderer;
  }

  /**
  * {@inheritdoc}
  */
  public function log($level, $message, array $context = []) {
    $levelLog = $this->config->get('log_google_chat_' . $level);
    $allowedMessageType = $this->config->get('allow_message_types');
    $isContentAllowed =
      $this->is_content_allowed( $allowedMessageType, $context['channel']);

    if($isContentAllowed || $levelLog){
      $message_format = $this->format_message_with_theme($level,$context,$message);
      $this->send_to_google_chat($message_format);
    }
  }

  public function send_to_google_chat($message){
    // Prepare the log message for sending to Google Chat.
    $message = [
      'text' => $message,
    ];

    $url = $this->config->get('google_chat_webhook');

    // Send the log message to Google Chat.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($message));
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);

    curl_close($ch);
  }

  public function format_message_with_theme($level,$context,$message){
    $log = [
      'level' => $level,
      'context' => $context,
      'message' => $message,
    ];

    // Send themed alert to the web server's log.
    if (\Drupal::hasService('theme.manager')) {
      $log_google_chat_theme_element = [
        '#theme' => 'log_google_chat_format',
        '#log' => $log,
      ];
      $message = $this->renderer->renderPlain($log_google_chat_theme_element);;
    }

    return $message;

  }

  public function is_content_allowed($allowedMessageTypeConfig, $messageTypeLog){
    $arrayAllowedMessage = explode(",", $allowedMessageTypeConfig);
    return in_array($messageTypeLog, $arrayAllowedMessage) ? true : false;
  }

}


