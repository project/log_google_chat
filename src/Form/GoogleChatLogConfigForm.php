<?php

namespace Drupal\log_google_chat\Form;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a setting UI for log_google_chat.
 */
class GoogleChatLogConfigForm extends ConfigFormBase {

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['log_google_chat.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'log_google_chat_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('log_google_chat.settings');
    $form['log_google_chat'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Error logging for each severity level.'),
      '#description' => $this->t('Check each severity level you want to get logged to the error log.'),
    ];
    foreach (RfcLogLevel::getLevels() as $severity => $description) {
      $key = 'log_google_chat_' . $severity;
      $form['log_google_chat'][$key] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Severity: @description', ['@description' => Unicode::ucfirst($description->render())]),
        '#default_value' => $config->get($key) ?: FALSE,
      ];
    }

    $form['log_google_chat']['google_chat_webhook'] = [
      '#type' => 'textarea',
      '#title' => 'Webhook url google chats',
      '#default_value' => $config->get('google_chat_webhook'),
    ];

    $form['log_google_chat']['allow_message_types'] = [
      '#type' => 'textarea',
      '#title' => 'Allowed message types ',
      '#default_value' => $config->get('allow_message_types'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('log_google_chat.settings');
    $userInputValues = $form_state->getUserInput();

    foreach (RfcLogLevel::getLevels() as $severity => $description) {
      $key = 'log_google_chat_' . $severity;
      $config->set($key, $userInputValues[$key]);
    }

    $config->set('google_chat_webhook', $userInputValues['google_chat_webhook']);
    $config->set('allow_message_types', $userInputValues['allow_message_types']);
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
