<?php

/**
 * @file
 * Drupal Module: Web Server Logging and Alerts.
 *
 * Sends logs and alerts to the google chat log.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Implements hook_help().
 */
function log_google_chat_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'log_google_chat.configuration':
      return '<p>' . t('Sends logs and alerts to the Google Chat\'s error log.') . '</p>';
  }
}

/**
 * Implements hook_theme().
 */
function log_google_chat_theme() {
  return [
    'log_google_chat_format' => [
      'variables' => ['log' => NULL],
    ],
  ];
}

/**
 * Prepares variables for log_google_chat format templates.
 *
 * Default template: log_google_chat-format.html.twig.
 */
function template_preprocess_log_google_chat_format(&$variables) {
  global $base_root;
  $config = \Drupal::config('system.site');
  $log_data = $variables['log'];
  $log_context = $variables['log']['context'];
  $severity_list = RfcLogLevel::getLevels();
  $message_body = new FormattableMarkup($log_data['message'], $log_context);
  $message = '';
  $message .= ' ▶️ *Severity:* ' . $severity_list[$log_data['level']] . PHP_EOL;
  $message .= ' ▶️ *Type* ' . $log_context['channel'] . PHP_EOL;
  $message .= ' ▶️ *Uid:* ' . $log_context['uid']. PHP_EOL;
  $message .= ' ▶️ *Referer:* ' . $log_context['referer'] . PHP_EOL;
  $message .= ' ▶️ *Message* ```' .  strip_tags($message_body) . '```';
  $variables['message'] = $message;
}


